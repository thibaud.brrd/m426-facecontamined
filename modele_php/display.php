<?php

require_once 'connexion.php';

function displayQueryAsTable($query)
{
    $output = "<table class=\"table dataTable my-0\" id=\"dataTable\">";
    $output .= "<thead>";
    $output .= "<tr>";
    foreach ($query[0] as $key => $value) {
        $output .= "<th>$key</th>";
    }
    $output .= "</tr>";
    $output .= "</thead>";
    $output .= "<tbody>";
    foreach ($query as $line) {
        $output .= "<tr>";
        foreach ($line as $key => $value) {
            $output .= "<td>$value</td>";
        }
        $output .= "</tr>";
    }
    $output .= "</tbody>";
    $output .= "<tfoot>";
    $output .= "<tr>";
    foreach ($query[0] as $key => $value) {
        $output .= "<td><strong>$key</strong></td>";
    }
    $output .= "</tr>";
    $output .= "</tfoot>";
    $output .= "</table>";

    return $output;
}

function pageSelector($numberOfElement, $limitPerPage, $noOfActualPage)
{
    $numberOfPage = ceil($numberOfElement / $limitPerPage);

    $output = "<ul>";
    if ($noOfActualPage > 4) {
        $output .= "<li><a href=\"./index.php?noPage=1\"> 1 </a></li>";
        $output .= "<li> ... </li>";
    }
    else if ($noOfActualPage > 3) {
        $output .= "<li><a href=\"./index.php?noPage=1\"> 1 </a></li>";
    }
    for ($i = $noOfActualPage - 2; $i <= $noOfActualPage + 2; $i++) {
        if ($i >= 1 && $i <= $numberOfPage) {
            if ($i == $noOfActualPage) {
                $output .= "<li><b> $i </b></li>";
            }
            else {
                $output .= "<li><a href=\"./index.php?noPage=$i\"> $i </a></li>";
            }
        }
    }
    if ($noOfActualPage < $numberOfPage - 3) {
        $output .= "<li> ... </li>";
        $output .= "<li><a href=\"./index.php?noPage=$numberOfPage\"> $numberOfPage </a></li>";
    }
    else if ($noOfActualPage < $numberOfPage - 2) {
        $output .= "<li><a href=\"./index.php?noPage=$numberOfPage\"> $numberOfPage </a></li>";
    }
    $output .= "</ul>";

    return $output;
}