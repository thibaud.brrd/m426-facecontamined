-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 11 juin 2020 à 14:25
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_facecontaminate`
--

--
-- Déchargement des données de la table `t_friends`
--

INSERT INTO `t_friends` (`idUser`, `idFriendUser`) VALUES
('Jojo', 'ValoXD12'),
('Jojo', 'Yvan31');

--
-- Déchargement des données de la table `t_meet`
--

INSERT INTO `t_meet` (`id`, `idUser`, `idFriendUser`, `startDate`, `endDate`) VALUES
(1, 'Jojo', 'Yvan31', '2020-03-24 16:00:00', '2020-03-24 17:30:00'),
(2, 'Jojo', 'ValoXD12', '2020-05-12 18:30:00', '2020-05-12 19:10:30'),
(3, 'Jojo', 'Yvan31', '2020-06-01 20:00:00', '2020-06-01 22:00:00');

--
-- Déchargement des données de la table `t_users`
--

INSERT INTO `t_users` (`login`, `email`, `password`, `firstname`, `lastname`, `idStatus`, `idEtat`) VALUES
('Yvan31', 'yvan.latour@outlook.com', '8185c8ac4656219f4aa5541915079f7b3743e1b5f48bacfcc3386af016b55320', 'Yvan', 'Latour', 1, 1),
('Jojo', 'John.Doe@Wesh.zebi', '8185c8ac4656219f4aa5541915079f7b3743e1b5f48bacfcc3386af016b55320', 'John', 'Doe', 1, 1),
('ValoXD12', 'Valorie.Doe@gmail.com', 'A7F7AE1986D455EBDFBF40BB1D0969645B4CC2CDAB8D66ECFA06D712DD252042', 'Valorie', 'Doe', 1, 3);

--
-- Déchargement des données de la table `t_virus`
--

INSERT INTO `t_virus` (`id`, `label`, `incubationDays`, `contaminateDuringIncubation`) VALUES
(1, 'SARS-CoV-2', 14, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
