<?php
require_once 'session_init.php';
require_once '../modele_php/users.php';
require_once '../modele_php/friends.php';
require_once '../modele_php/display.php';
$me = $_SESSION['me'];

$login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
$firstname = filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_STRING);
$lastname = filter_input(INPUT_POST, "lastname", FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$confirme_password= filter_input(INPUT_POST, "confirme_password", FILTER_SANITIZE_STRING);
$btn = filter_input(INPUT_POST,"submit");
$mail= filter_input(INPUT_POST, "mail", FILTER_SANITIZE_STRING);


if($btn == "inscription")
{ 
    
     $check = getUser($login);

      if($check != null)
        {  
          $show = "l'utilisateur existe deja";
        }
      else
        {               
          addUser($login, $password, $confirme_password, $firstname, $lastname, $mail);
          $me = $login;
          $_SESSION['me'] = $me;
          header("Location: index.php");
          exit;
        }       
  }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Register - Brand</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Animated-CSS-Waves-Background-SVG.css">
    <link rel="stylesheet" href="assets/css/Animated-rainbow-shadow.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/The-Matrix-Display.css">
    <link rel="stylesheet" href="assets/css/untitled.css">
</head>

<body class="bg-gradient-primary">
    <div class="card"></div>
    <div class="login-dark">
        <form method="post">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group"><input class="form-control" type="text" name="firstname" placeholder="firstname"></div>
            <div class="form-group"><input class="form-control" type="text" name="lastname" placeholder="lastname"></div>
            <div class="form-group"><input class="form-control" type="text" name="login" placeholder="Pseudo"></div>
            <div class="form-group"><input class="form-control" type="mail" name="mail" placeholder="Email"></div> 
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Mot de passe"></div><input class="form-control" type="password" name="confirme_password" placeholder="Confirmer le mot de passe">
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="submit" value="inscription">Connection</button></div><a class="forgot" href="#">Forgot your email or password?</a></form>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/The-Matrix-Display.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>